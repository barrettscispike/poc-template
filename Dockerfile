FROM python:3.7.3-alpine3.9 AS base

RUN mkdir -p /app
WORKDIR /app

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

CMD ["python3", "__main__.py"]