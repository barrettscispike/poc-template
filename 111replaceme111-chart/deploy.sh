#!/usr/bin/env sh

THIS_DIR="$(cd "$(dirname "$0")"; pwd)"

ENV=${ENV:-dev}

helm upgrade \
  <%= customComponentName %> \
  "$THIS_DIR" \
  --set <%= customComponentName %>.env=$ENV \
  --install \
  --debug \
  --atomic \
  --cleanup-on-fail \
  $@
