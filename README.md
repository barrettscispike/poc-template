# `<%= customComponentName %>`

This is the <%= customComponentName %> project for <%= projectName %>.

Version: 0.1.0-pre.0

## Repository Version, Branch & Release Management
Please review https://gitlab.com/northscaler-public/release-management for information about how this project's versions, branches and releases are managed.
